﻿<%@ Page Title="Register" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="PL.Account.Register" %>

<asp:Content runat="server" ID="BodyContent" ContentPlaceHolderID="MainContent">
    <h2><%: Title %>.</h2>
    <p class="text-danger">
        <asp:Literal runat="server" ID="ErrorMessage" />
    </p>

    <div class="form-horizontal">
        <h4>Create a new account.</h4>
        <hr/>
        <asp:ValidationSummary runat="server" CssClass="text-danger" />
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="First_Name" CssClass="col-md-2 control-label" ID="Label1">שם פרטי</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="First_Name" CssClass="form-control"  />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Last_name" CssClass="col-md-2 control-label" ID="Label2" style="left: 0px; top: 17px">שם משפחה</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Last_name" CssClass="form-control"  />
                </div>
            </div>
                <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="UserID" CssClass="col-md-2 control-label" ID="Label5">תעודת זהות</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="UserID" CssClass="form-control"  />
            </div>
            </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="First_Name" CssClass="col-md-2 control-label" ID="Label3">עיר</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="City" CssClass="form-control"  />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="First_Name" CssClass="col-md-2 control-label" ID="Label4">גיל</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Age" CssClass="form-control" ControlToValidate="number" ErrorMessage="גיל לא תקין" />
            </div>
        </div>
                        <div class="form-group">
                <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">מין</asp:Label>
                                <div class="col-md-10">
        <asp:RadioButtonList id=RadioButtonList1 runat="server">
<asp:ListItem> זכר</asp:ListItem>
<asp:ListItem>נקבה</asp:ListItem>
</asp:RadioButtonList>
                                    </div>
                    </div>
                <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Email" CssClass="col-md-2 control-label">דוא"ל</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Email" CssClass="form-control" TextMode="Email"/>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Email"
                    CssClass="text-danger" ErrorMessage="חסר דואר אלקטרוני" />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="Password" CssClass="col-md-2 control-label">סיסמא</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="Password" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="Password"
                    CssClass="text-danger" ErrorMessage="חסר סיסמא" />
            </div>
        </div>
        <div class="form-group">
            <asp:Label runat="server" AssociatedControlID="ConfirmPassword" CssClass="col-md-2 control-label">אימות סיסא</asp:Label>
            <div class="col-md-10">
                <asp:TextBox runat="server" ID="ConfirmPassword" TextMode="Password" CssClass="form-control" />
                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="The confirm password field is required." />
                <asp:CompareValidator runat="server" ControlToCompare="Password" ControlToValidate="ConfirmPassword"
                    CssClass="text-danger" Display="Dynamic" ErrorMessage="נא לאמת סיסמא" />
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-offset-2 col-md-10">
                <asp:Button runat="server" OnClick="CreateUser_Click" Text="הרשם" CssClass="btn btn-default" />
            </div>
        </div>
    </div>
</asp:Content>
