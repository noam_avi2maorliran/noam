﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using ServiceLayer;
using BLBack;
using PL.Models;

namespace PL.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            RegisterHyperLink.NavigateUrl = "Register";
            // Enable this once you have account confirmation enabled for password reset functionality
            // ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];
            var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            if (!String.IsNullOrEmpty(returnUrl))
            {
                RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            }
        }

        protected void LogIn(object sender, EventArgs e)
        {
            string error = "alert(\"שם משתמש או סיסמא שגויים \");";
            string e_mail = Email.Text;
            string pass = Password.Text;
            service ser = service.getInstance();
            Session["User"] = ser.login(e_mail, pass);
            if (Session["User"]== null)
                ScriptManager.RegisterStartupScript(this, GetType(), "ServerControlScript", error, true);
            else
                Response.Redirect("welcom.aspx");

        }
    }
}