﻿using System;
using System.Linq;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using BLBack;
using ServiceLayer;
using PL.Models;

namespace PL.Account
{
    public partial class Register : Page
    {
        protected void CreateUser_Click(object sender, EventArgs e)
        {
            service ser = service.getInstance();
            bool gender = true;
            string username = Email.Text;
            string fName = First_Name.Text;
            string lName = Last_name.Text;
            string curCity = City.Text;
            long ID = Int64.Parse(UserID.Text);
            int ageCus = Int32.Parse(Age.Text);
            if (RadioButtonList1.SelectedIndex > -1)
            {
                gender = (RadioButtonList1.SelectedItem.Text.Equals("זכר"));
            }
            string pass = Password.Text;
            string script = "alert(\"user name = " + username + " first name = " + fName + " last name = " + lName + " city = " + curCity + " age = " + ageCus + "\");";
            ScriptManager.RegisterStartupScript(this, GetType(),
                                  "ServerControlScript", script, true);
            UserInfo per = new UserInfo(username,pass,1);
            Person user = new Person(ID, fName, lName, curCity, ageCus, gender, per);
            ser.addPerson(user);
            Session["User"] = user;
            Response.Redirect("welcom.aspx");
        }
    }
}