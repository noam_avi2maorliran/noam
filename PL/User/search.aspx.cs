﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ServiceLayer;
using BLBack;

namespace PL.User
{
    public partial class search : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            service ser = service.getInstance();
            List < Coupon > list = ser.searchCoupon(1);
            DataGrid1.DataSource = ConvertListToDataTable(list);
            DataGrid1.DataBind();
        }
        static DataTable ConvertListToDataTable(List<Coupon> list)
        {
            int size = list.Count;
            string[] coupon = new string[9];
            // New table.
            DataTable table = new DataTable();
            // Get max columns.
            // Add columns.

            table.Columns.Add("שם");
            table.Columns.Add("תאריך התחלה");
            table.Columns.Add("תאריך סיום");
            table.Columns.Add("מותג");
            table.Columns.Add("מלאי");
            table.Columns.Add("כמות שהוזמן");
            table.Columns.Add("מחיר מקורי");
            table.Columns.Add("מחיר חדש");
            table.Columns.Add("תיאור קופון");

            // id name startdate endate brand category stock numoforders original new price descripton bussiness id

            // Add rows.
            for (int i = 0; i < size; i++)
            {
                coupon[0] = list.ElementAt(i).name;
                coupon[1] = list.ElementAt(i).startDate.ToString();
                coupon[2] = list.ElementAt(i).endDate.ToString();
                coupon[3] = list.ElementAt(i).Brand;
                coupon[4] = list.ElementAt(i).stock.ToString();
                coupon[5] = list.ElementAt(i).NumOfOrders.ToString();
                coupon[6] = list.ElementAt(i).OriginalPrice.ToString();
                coupon[7] = list.ElementAt(i).NewPrice.ToString();
                coupon[8] = list.ElementAt(i).Descriptions;
                table.Rows.Add(coupon);
            }
            return table;
        }
    }
}