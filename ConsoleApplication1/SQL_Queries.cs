﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using BLBack;
using System.Device.Location;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ConsoleApplication1
{
    public class SQL_Queries
    {

        static SQL_Queries singleton = new SQL_Queries();
        SqlConnection sqlCon;
        private SQL_Queries()
        {
            try
            {
                sqlCon = new SqlConnection();
                string location = "C:\\Users\\avichay\\Desktop\\NOAM2\\NOAM\\ConsoleApplication1";
                sqlCon.ConnectionString = @"Data Source=(LocalDB)\v11.0;AttachDbFilename=" + location + "\\NOAM.mdf;Integrated Security=True";
                Console.Write(sqlCon.ConnectionString);
            }
            catch (Exception e)
            {
            }
        }
        public static SQL_Queries getInstance()
        {
            return singleton;
        }
        public void addUser(long ID, string firstName, string lastName, string City, int age, bool gender, string userName, string password, int permission, int[] categories)
        {
            int boolGender = gender ? 1 : 0;
            string command = "INSERT INTO Users VALUES (" + ID + ",'" + firstName + "','" + lastName + "','" + City + "',NULL," + age + ",'" + boolGender + "')";
            string command1 = "INSERT INTO PERMISSION VALUES (" + ID + ",'" + userName + "','" + password + "','" + permission + "')";
            try
            {
                SqlCommand exSQL = new SqlCommand(command, sqlCon);
                sqlCon.Open();
                exSQL.ExecuteNonQuery();
                exSQL.CommandText = command1;
                exSQL.ExecuteNonQuery();
                foreach (int cat in categories)
                {
                    string command2 = "INSERT INTO UsersPreferences VALUES('" + ID + "','" + cat + "')";
                    exSQL.CommandText = command2;
                    exSQL.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
        }
        public void addBusiness(int ID, string name, string description, long ownerID, int[] categories,string city)
        {
            string command = "INSERT INTO Business VALUES ('" + ID + "','" + name + "',geography::Point(47.78100, -122.33900, 4326),'" + description + "','" + city + "')";
            string command1 = "INSERT INTO BusinessForOwners VALUES ('" + ownerID + "','" + ID + "')";
            try
            {
                SqlCommand exSQL = new SqlCommand(command, sqlCon);
                sqlCon.Open();
                exSQL.ExecuteNonQuery();
                exSQL.CommandText = command1;
                exSQL.ExecuteNonQuery();
                foreach (int cat in categories)
                {
                    string command2 = "INSERT INTO CategoriesForBusinesses VALUES('" + ID + "','" + cat + "')";
                    exSQL.CommandText = command2;
                    exSQL.ExecuteNonQuery();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
        }
        public void addCoupon(string name, string startDate, string endDate, string brand, int categoryID, int stock, int originalPrice, int newPrice, string description,int businessID)
        {
            long maxID = getMaxValue("Coupons", "ID");
            string command = "INSERT INTO Coupons VALUES (" + (maxID + 1) + ",'" + name + "','" + startDate + "','" + endDate + "','" + brand + "'," + categoryID + "," + stock + "," + 0 + "," + originalPrice + "," + newPrice + ",'" + description + "'," + 0 +"," + businessID + ")";
            try
            {
                SqlCommand exSQL = new SqlCommand(command, sqlCon);
                sqlCon.Open();
                exSQL.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
        }
        public bool removeFrom(string table, string columnName, string key)
        {
            bool ans = true;
            int before = getNumOf(table);
            string command = "DELETE FROM " + table + " WHERE " + columnName + "='" + key + "'"; ;
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try
            {
                sqlCon.Open();
                exSQL.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                ans = false;
            }
            sqlCon.Close();
            int after = getNumOf(table);
            return (before != after);
        }
        public bool isUserExist(string ID)
        {
            bool ans = true;
            string command = "SELECT ID FROM Users where ID= " + ID;
            SqlDataReader rea = null;
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try
            {
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                rea.Read();
                ans = rea.HasRows;
            }
            catch (Exception e)
            {
                ans = false;
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            return ans;
        }
        public long getMaxValue(string table, string column)
        {
            int ans = 0;
            SqlDataReader rea = null;
            string command = "SELECT MAX(" + column + ") FROM " + table;
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try
            {
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                rea.Read();
                ans = Int32.Parse(rea[0].ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            return ans;
        }
        public int getNumOf(string table)
        {
            int ans = 0;
            SqlDataReader rea = null;
            string command = "SELECT Count (*) FROM " + table;
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try
            {
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                rea.Read();
                ans = Int32.Parse(rea[0].ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            return ans;
        }
        public List<Coupon> searchCoupon(string column, string value)
        {
            List<Coupon> coupons = new List<Coupon>();
            SqlDataReader rea = null;
            string command = "SELECT * FROM Coupons WHERE "+column+"='"+value+"'";
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try{
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                while (rea.Read())
                {
                    coupons.Add(new Coupon(Int32.Parse(rea["ID"].ToString()) ,rea["Name"].ToString() ,DateTime.Parse(rea["StartDate"].ToString()),DateTime.Parse(rea["EndDate"].ToString()),rea["Brand"].ToString(), Int32.Parse(rea["categoryID"].ToString()) ,Int32.Parse(rea["Stock"].ToString()) ,Int32.Parse(rea["NumOfOrders"].ToString()), Int32.Parse(rea["OriginalPrice"].ToString()) ,rea["NewPrice"].ToString(),Boolean.Parse(rea["Approved"].ToString())));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            return coupons;
        }
        public List<Coupon> searchCouponByCity(string value)
        {
            List<Business> bus = searchBusinessByCity(value);
            List<Coupon> coupons = new List<Coupon>();
            SqlDataReader rea = null;
            foreach (Business b in bus)
            {
                string command = "SELECT * FROM Coupons WHERE " + "BusinessID" + "='" + b._id + "'";
                SqlCommand exSQL = new SqlCommand(command, sqlCon);
                try
                {
                    sqlCon.Open();
                    rea = exSQL.ExecuteReader();
                    while (rea.Read())
                    {
                        coupons.Add(new Coupon(Int32.Parse(rea["ID"].ToString()), rea["Name"].ToString(), DateTime.Parse(rea["StartDate"].ToString()), DateTime.Parse(rea["EndDate"].ToString()), rea["Brand"].ToString(), Int32.Parse(rea["categoryID"].ToString()), Int32.Parse(rea["Stock"].ToString()), Int32.Parse(rea["NumOfOrders"].ToString()), Int32.Parse(rea["OriginalPrice"].ToString()), rea["NewPrice"].ToString(), Boolean.Parse(rea["Approved"].ToString())));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("hi");
                }
                sqlCon.Close();
            }
            
            return coupons;
        }
        public List<Business> searchBusinessByCity(string value) 
        {
            List<Business> businesses = new List<Business>();
            SqlDataReader rea = null;
            string command = "SELECT * FROM Business WHERE " + "City" + "='" + value + "'";
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            int[] cat = {};
            try
            {
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                while (rea.Read())
                {
                    businesses.Add(new Business(Int32.Parse(rea["ID"].ToString()), rea["Name"].ToString(), -1, rea["B_Description"].ToString(), cat, rea["City"].ToString()));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            return businesses;
        }
        public List<Business> searchBusinessByLocation(GeoCoordinate coordinate)
        {
            throw new NotImplementedException();
        }
        public List<Business> searchBusinessByCategory(string column, string value)
        {
            List<Business> businesses = new List<Business>();
            SqlDataReader rea = null;
            string command = "SELECT * FROM Coupons WHERE " + column + "='" + value + "'";
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try
            {
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                while (rea.Read())
                {
                    int businessID=Int32.Parse(rea["ID"].ToString());
                    int ownerID=getOwnerIDForBusiness(businessID);
                    int[] categories=getCategoriesForBusiness(businessID);
                    businesses.Add(new Business(businessID, rea["Name"].ToString(), ownerID, rea["B_Description"].ToString(), categories, rea["City"].ToString()));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            return businesses;
        }
        private int[] getCategoriesForBusiness(int businessID)
        {
            List<int> temp=new List<int>();
            int i=0;
            string command = "SELECT CategoryID FROM CategoriesForBusinesses where BusinessID= " + businessID;
            SqlDataReader rea = null;
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try
            {
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                while (rea.Read())
                {
                    temp.Add(Int32.Parse(rea["CategoryID"].ToString()));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            int[] categories = new int[temp.Count];
            for (i = 0; i < categories.Length; i++)
            {
                categories[i] = temp.ElementAt(i);
            }
                return categories;
        }
        private int getOwnerIDForBusiness(int businessID)
        {
            int ans = 0;
            SqlDataReader rea = null;
            string command = "SELECT OwnerID FROM BusinessForOwners WHERE BusinessID="+businessID+"";
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try
            {
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                rea.Read();
                ans = Int32.Parse(rea[0].ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            return ans;
        }
        public void editCoupon(int p1, string p2, string p3, string p4, string p5, int p6, int p7, int p8, int p9, int p10, string p11, bool p12)
        {
            string[] arguments = { p2, p3, p4, p5, p6.ToString(), p7.ToString(), p8.ToString(), p9.ToString(), p10.ToString(), p11, p12.ToString() };
            string[] fields = { "Name", "StartDate", "EndDate", "Brand", "CategoryID", "Stock", "NumOfOrders", "OriginalPrice", "NewPrice", "Descriptions", "Approved" };
            generalEditTable("Coupons", "ID", p1.ToString(), arguments, fields);
        }
        private void generalEditTable(string tableName, string columnName, string value, string[] arguments, string[] fields)
        {
            string command = "UPDATE " + tableName + " SET ";
            for (int i = 0; i < arguments.Length; i++)
            {
                command += fields[i] + "='" + arguments[i] + "'";
                if (i != arguments.Length - 1)
                    command += ", ";
            }
            command += "WHERE " + columnName + "='" + value + "'";
            try
            {
                SqlCommand exSQL = new SqlCommand(command, sqlCon);
                sqlCon.Open();
                exSQL.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
        }
        public List<Business> searchBusinessByCategory(int category)
        {
            List<Business> businesses = new List<Business>();
            SqlDataReader rea = null;
            string command = "SELECT businessID FROM CategoriesForBusinesses WHERE categoryID=" + category + "";
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try
            {
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                while (rea.Read())
                {
                    int businessID = Int32.Parse(rea["ID"].ToString());
                    int ownerID = getOwnerIDForBusiness(businessID);
                    int[] categories = getCategoriesForBusiness(businessID);
                    businesses.Add(new Business(businessID, rea["Name"].ToString(), ownerID, rea["B_Description"].ToString(), categories, rea["City"].ToString()));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            return businesses;
        }
        public void editBusiness(int p1, string p2, GeoCoordinate geoCoordinate, string p3, long p4, int[] p5)
        {
            string[] arguments = { p2, p3 };
            string[] fields = { "Name", "B_Description" };
            generalEditTable("businesses", "ID", p1.ToString(), arguments, fields);
            string[] arguments1 = { p4.ToString() };
            string[] fields1 = { "OwnerID" };
            generalEditTable("BusinessForOwners", "businessID", p1.ToString(), arguments1, fields1);
        }
        public Person getUser(string username, string password)
        {
            long ID;
            string command = "SELECT ID FROM Permission where username='" + username + "' and pass_word='" + password + "'";
            SqlDataReader rea = null;
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try
            {
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                rea.Read();
                ID=long.Parse(rea["ID"].ToString());
            }
            catch (Exception e)
            {
                ID = 0;
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            if (ID == 0)
                return null;
            return getUser(ID,getPermission(ID),getCategoriesForUser(ID));
        }
        private int[] getCategoriesForUser(long ID)
        {
            List<int> temp = new List<int>();
            int i = 0;
            string command = "SELECT CategoryID FROM UsersPreferences where UserID= " + ID + "";
            SqlDataReader rea = null;
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try
            {
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                while (rea.Read())
                {
                    temp.Add(Int32.Parse(rea["CategoryID"].ToString()));
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            int[] categories = new int[temp.Count];
            for (i = 0; i < categories.Length; i++)
            {
                categories[i] = temp.ElementAt(i);
            }
            return categories;
        }
        private UserInfo getPermission(long ID)
        {
            UserInfo permission;
            SqlDataReader rea = null;
            string command = "SELECT * FROM Permission WHERE ID=" + ID + "";
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try
            {
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                rea.Read();
                permission = new UserInfo(rea["userName"].ToString(),rea["pass_Word"].ToString(),Int32.Parse(rea["permissionLevel"].ToString()));
            }
            catch (Exception e)
            {
                return null;
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            return permission;
        }
        private Person getUser(long ID, UserInfo permission, int[] categories)
        {
            Person person;
            SqlDataReader rea = null;
            string command = "SELECT * FROM Users WHERE ID=" + ID + "";
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try
            {
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                rea.Read();
                person = new Person(ID, rea["firstName"].ToString(), rea["lastName"].ToString(), rea["City"].ToString(), Int32.Parse(rea["Age"].ToString()), Boolean.Parse(rea["Gender"].ToString()), permission);
            }
            catch (Exception e)
            {
                return null;
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            return person;
        }
        public Purchase newPurchase(long personId, int couponId)
        {
            int maxID = (int)getMaxValue("CopunForUsers", "CopunCode");
            int couponCode = maxID+1;
            DateTime now = DateTime.Now.Date;
            string now1 = now.Date.ToString("yyyy-MM-dd");
            //now1 = now1.Substring(0, 10);//check date!!!!!!!!!!!!!!!!!!!!!
            Purchase p = new Purchase(couponCode,couponId,personId,now);
            if (isCouponAvailable(couponId)) {
                string command = "INSERT INTO CopunForUsers VALUES ('" + (maxID + 1) + "','" + personId + "','" + couponId + "','" + now1 + "','False')";
                try
                {
                    SqlCommand exSQL = new SqlCommand(command, sqlCon);
                    sqlCon.Open();
                    exSQL.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    Console.WriteLine("hi");
                }
                sqlCon.Close(); 
            }
            couponReserved(couponId);
            return p;
        }
        private void couponReserved(int couponId)
        {
            string command = "UPDATE " + "Coupons" + " SET NumOfOrders='"+ (getTableField("Coupons","NumOfOrders",couponId)+1) + "' WHERE ID='"+couponId+"'";
            try
            {
                SqlCommand exSQL = new SqlCommand(command, sqlCon);
                sqlCon.Open();
                exSQL.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
        }
        private int getTableField(string table, string field, int where)
        {
            int ans = 0;
            SqlDataReader rea = null;
            //SELECT Address FROM Customers Where CustomerID=1
            string command = "SELECT " + field + " FROM " + table + " WHERE ID=" + where.ToString();
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try
            {
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                rea.Read();
                ans = Int32.Parse(rea[0].ToString());
            }
            catch (Exception e)
            {
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            return ans;
        }
        private bool isCouponAvailable(int couponId)
        {
            bool ans = false;
            string command = "SELECT * FROM Coupons where ID= " + couponId;
            SqlDataReader rea = null;
            SqlCommand exSQL = new SqlCommand(command, sqlCon);
            try
            {
                sqlCon.Open();
                rea = exSQL.ExecuteReader();
                rea.Read();
                if ( (Int32.Parse(rea["Stock"].ToString())) > (Int32.Parse(rea["NumOfOrders"].ToString())) ) { 
                    ans=true;
                }
            }
            catch (Exception e)
            {
                ans = false;
                Console.WriteLine("hi");
            }
            sqlCon.Close();
            return ans;
        }
    }
}