﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using ConsoleApplication1;

namespace NUnit_Tests
{
    [TestFixture]
    public class NUnit_Tests
    {
        private SQL_Queries DB;
        [SetUp]
        public void setUp()
        {
            DB = SQL_Queries.getInstance();
        }
        [Test]
        //_returns_@before.getNumOfUser+1_when_given_getNumOfUser()
        public void addUser()
        {
            int before = DB.getNumOf("Users");
            int[] arr = { 0, 1 };
            long id = DB.getMaxValue("Users", "ID")+1;
            Console.WriteLine(id);
            DB.addUser(id, "Maor", "Maor", "Maor", 25, true, id+"", "Maor", 3, arr);
            Assert.AreEqual(before + 1, DB.getNumOf("Users"));
            Assert.IsTrue(DB.isUserExist(id + ""));
        }
        [Test]
        public void addUser_Shouldnt_add_a_usuer_with_illegal_Id()
        {
            int before = DB.getNumOf("Users");
            int[] arr = { 0, 1 };
            long id = DB.getMaxValue("Users", "ID");
            DB.addUser(id, "Maor", "Maor", "Maor", 25, true, "Maor", "Maor", 3, arr);
            Assert.AreEqual(before, DB.getNumOf("Users"));
        }
        [Test]
        public void addBussiness()
        {
            int before = DB.getNumOf("Business");
            int[] arr = { 0, 1 };
            int id = DB.getNumOf("Business");
            DB.addBusiness(id, "BGU", "BGU", 111222300, arr,"Beer Sheva");
            Assert.AreEqual(before + 1, DB.getNumOf("Business"));
        }
        [Test]
        public void addCoupun()
        {
            long before = DB.getNumOf("Coupons");
            DB.addCoupon("ham", "2014-10-3", "2015-10-2", "fifa", 1, 50, 500, 250, "New price shoes",1);
            Assert.AreEqual(before + 1, DB.getNumOf("Coupons"));
        }
        [Test]
        public void addCoupun_check_constrain_not_adding_when_original_price_lower_or_equal_to_new_price()
        {
            int before = DB.getNumOf("Coupons");
            DB.addCoupon("ham", "15/04/2014", "15/04/2014", "fifa", 1, 50, 100, 250, "100 is less then 250",1);
            Assert.AreEqual(before, DB.getNumOf("Coupons"));
        }
        [Test]
        public void addCoupun_check_not_adding_when_category_does_not_exist_due_its_foreign_key()
        {
            int before = DB.getNumOf("Coupons");
            DB.addCoupon("ham", "15/04/2014", "15/04/2014", "fifa", 50000000, 50, 100, 250, "there is no category 50000000",1);
            Assert.AreEqual(before, DB.getNumOf("Coupons"));
        }
        [Test]
        public void removeFrom_should_return_false_because_trying_to_remove_where_others_has_its_forein_key()
        {
            bool ans = DB.removeFrom("Categories", "ID", "1");
            bool expected = false;
            Assert.AreEqual(ans, expected);
        }
        [Test]
        public void removeFrom()
        {
            int beforeAdding = DB.getNumOf("Users");
            int[] arr = { 0, 1 };
            long id = DB.getMaxValue("Users", "ID")+1;
            DB.addUser(id, "Maor", "Maor", "Maor", 25, true, "Maor", "Maor", 3, arr);
            int afterAdding = DB.getNumOf("Users");
            bool ans = DB.removeFrom("Users", "ID", id + "");
            int afterDeleting = DB.getNumOf("Users");
            Assert.IsFalse(DB.isUserExist(id + ""));
            Assert.IsTrue(ans);
            Assert.AreEqual(beforeAdding, afterDeleting);
            Assert.AreEqual(afterAdding, afterDeleting + 1);
        }

        [Test]
        public void addBusiness_not_adding_duplicate_ID()
        {
            int before = DB.getNumOf("Business"); 
            long id = DB.getMaxValue("Business","ID");
            int [] arr = { 0, 1 };
            DB.addBusiness((int)(id), null,null,DB.getMaxValue("Users","ID"),arr,"Hadera");
            int after = DB.getNumOf("Business"); 
            Assert.AreEqual(before, after);
        }
        [Test]
        public void removeUser_cant_remove_because_user_does_not_exist()
        {
            int before = DB.getNumOf("Users");
            long id = DB.getMaxValue("Users", "ID");
            id += 1;
            int[] arr = { 0, 1 };
            bool ans = DB.removeFrom("Users", "ID", id + "");
            int after = DB.getNumOf("Users");
            Assert.AreEqual(before, after);
            Assert.IsFalse(ans);
        }

        [TearDown]
        public void TearDown()
        {
            DB = null;
        }
    }
}
