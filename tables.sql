﻿create table Users (
ID numeric(9,0) primary key,
FirstName varchar(10) not null,
LastName varchar(10) not null,
City varchar(20) not null,
CurrentLocation  geography,
Age int not null,
Gender bit not null,
check(age>=12 and age<=70)
);

create table Permission (
ID numeric(9,0) FOREIGN KEY REFERENCES Users(ID) on update cascade on delete no action,
UserName varchar(20) primary key,
Pass_word varchar(10) not null,
PermissionLevel int not null
);

create table Categories (
ID int primary key,
Name varchar (10) not null
);

create table Business(
ID int primary key,
Name varchar(15) not null,
Location  geography not null,
B_Description varchar(100)
);

create table CategoriesForBusinesses(
BusinessID int FOREIGN KEY REFERENCES Business(ID) on update cascade on delete no action,
CategoryID int FOREIGN KEY REFERENCES Categories(ID) on update cascade on delete no action,
primary Key (BusinessID,CategoryID) 
);

create table BusinessForOwners (
OwnerID numeric(9,0) FOREIGN KEY REFERENCES Users(ID) on update cascade on delete no action,
BusinessID int FOREIGN KEY REFERENCES Business(ID) on update cascade on delete no action,
PRIMARY KEY (OwnerID,BusinessID)
);

create table Coupons(
ID int primary key,
Name varchar(15) not null,
StartDate date not null,
EndDate date not null,
Brand varchar(20),
CategoryID int FOREIGN KEY REFERENCES Categories(ID) on update cascade on delete no action,
Stock int not null,
NumOfOrders int default 0 check (NumOfOrders>=0),
OriginalPrice int not null,
NewPrice int not null,
Descriptions varchar(100),
Approved bit not null default 0
);

create table CopunForUsers (
CopunCode int primary key,
UserID numeric(9,0) FOREIGN KEY REFERENCES Users(ID) on update cascade on delete no action,
CouponID int FOREIGN KEY REFERENCES Coupons(ID) on update cascade on delete no action,
OrderDate date not null default getdate(),
IsUsed bit not null default 0
);

create table FriendsForUser (
UserID numeric(9,0) FOREIGN KEY REFERENCES Users(ID) on update no action on delete no action,
FriendID numeric(9,0) FOREIGN KEY REFERENCES Users(ID) on update no action on delete no action,
FriendsSince date not null default getdate(),
primary key (UserID,FriendID) 
);

create table CouponsRating (
UserID numeric(9,0) FOREIGN KEY REFERENCES Users(ID) on update cascade on delete no action,
CouponID int FOREIGN KEY REFERENCES Coupons(ID) on update cascade on delete no action,
Rating int not null check(Rating>=1 and Rating<=10),
RateDate date not null default getdate(),
Note varchar(100),
primary Key (UserID,CouponID) 
);

create table UsersPreferences (
UserID numeric(9,0) FOREIGN KEY REFERENCES Users(ID) on update cascade on delete no action,
CategoryID int FOREIGN KEY REFERENCES Categories(ID) on update cascade on delete no action,
primary Key (UserID,CategoryID) 
);