﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BLBack;
using ConsoleApplication1;
using System.Device.Location;
using System.Net.Mail;

namespace ServiceLayer
{
    public class service
    {
        static service singleton = new service();
        private SQL_Queries itsSQLQ;

        private service()
        {
            itsSQLQ = SQL_Queries.getInstance();
        }

        public static service getInstance()
        {
            return singleton;
        }

        public void addCoupon(Coupon coupon,int businessID)
        {
            itsSQLQ.addCoupon(coupon.name, coupon.startDate.ToString(), coupon.endDate.ToString(), coupon.Brand, coupon.categoryID, coupon.stock, coupon.OriginalPrice, coupon.NewPrice, coupon.Descriptions, businessID);
        }
        public void addPerson(Person p) {
            int[] categories = {0,1};
            itsSQLQ.addUser(p._id,p._firstName,p._lastName,p._city,p._age,p._gender,p.per._userName,p.per._password,1,categories);
        }

        //public void authorizeCoupon(int couponID);
        
        public void editCoupon(Coupon coupon)
        {
            itsSQLQ.editCoupon(coupon.couponID, coupon.name, coupon.startDate.ToString(), coupon.endDate.ToString(), coupon.Brand, coupon.categoryID, coupon.stock, coupon.NumOfOrders, coupon.OriginalPrice, coupon.NewPrice, coupon.Descriptions, coupon.Approved);
        }
        public void removeCoupon(int couponID)
        {
            itsSQLQ.removeFrom("Coupons", "ID", couponID.ToString());
        }
        public List<Coupon> searchCoupon(int catrgoryID)
        {
            List<Coupon> coupons=itsSQLQ.searchCoupon("CategoryID",catrgoryID.ToString());
            return coupons;
        }
        public List<Coupon> searchCoupon(string city)
        {
            List<Coupon> coupons = itsSQLQ.searchCouponByCity(city);
            return coupons;
        }
        public void addBusiness(Business bussi)
        {
            itsSQLQ.addBusiness(bussi._id, bussi.name, bussi.description, bussi._ownerID, bussi.categories, bussi.city);
        }
        public List<Business> serachBusiness(GeoCoordinate coordinate)
        {
            List<Business> businesses = itsSQLQ.searchBusinessByLocation(coordinate);
            return businesses;
        }
        public List<Business> serachBusiness(int category)
        {
            List<Business> businesses = itsSQLQ.searchBusinessByCategory(category);
            return businesses;
        }
        public void editBusiness(Business bussi)
        {
            itsSQLQ.editBusiness(bussi._id, bussi.name, bussi.location, bussi.description, bussi._ownerID, bussi.categories);
        }
        public Person login(string username, string password)
        {
            return itsSQLQ.getUser(username,password);
        }
        public Purchase reserveCoupon(long personId, int couponId)
        {
            Purchase p = itsSQLQ.newPurchase(personId, couponId);
            return p;
        }
    }
}
