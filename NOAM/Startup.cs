﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(NOAM.Startup))]
namespace NOAM
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
