﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLBack
{
    public class Person
    {
        public long _id {get; set;}
        public string _firstName { get; set; }
        public string _lastName { get; set; }
        public string _city { get; set; }
        public int _age { get; set; }
        public bool _gender { get; set; }
        public UserInfo per { get; set; }
        //public Location
        public Person(long id,string firstName,string lastName, string city,int age,bool gender,UserInfo per){
            _id = id;
            _firstName = firstName;
            _lastName = lastName;
            _city = city;
            _age = age;
            _gender = gender;
            this.per = per;
        }
        public Person() { }
    }
}
