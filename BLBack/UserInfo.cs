﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLBack
{
    public class UserInfo
    {
        public string _userName { get; set; }
        public string _password { get; set; }
        public int _permission { get; set; }
        public UserInfo(string userName, string password, int permission)
        {
            _userName = userName;
            _password = password;
            _permission = permission;
        }
    }
}
