﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLBack
{
    public class Purchase
    {
        public int _couponCode { get; set; }
        public int _couponID { get; set; }
        public long _userID { get; set; }
        public DateTime _orderDate { get; set; }
        public bool _isUsed { get; set; }
        public Purchase(int couponCode, int couponId, long userID, DateTime orderDate)
        {
            _couponCode = couponCode;
            _couponID = couponId;
            _userID = userID;
            _orderDate = orderDate;
            _isUsed = false;
        }
    }
}
