﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Device.Location;

namespace BLBack
{
    public class Business
    {
        public int _id {get; set;}
        public string name { get; set; }
        public long _ownerID { get; set; }
        public string description { get; set; }
        public int[] categories { get; set; }
        public GeoCoordinate location { get; set; }
        public string city { get; set; }
        public Business(int id,string name,long _ownerID, string description, int[] categories,string city)
        {
            _id = id;
            this.name = name;
            this._ownerID = _ownerID;
            this.description = description;
            this.categories = categories;
            this.location = null;
            this.city = city;
        }
    }
}
