﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLBack
{
    public class Coupon
    {
        public int couponID { get; set; }
        public string name { get; set; }
        public DateTime startDate { get; set; }
        public DateTime endDate { get; set; }
        public String Brand { get; set; }
        public int categoryID { get; set; }
        public int stock { get; set; }
        public int NumOfOrders { get; set; }
        public int OriginalPrice { get; set; }
        public int NewPrice { get; set; }
        public string Descriptions { get; set; }
        public bool Approved { get; set; }

        public Coupon(int couponID, string name, DateTime start, DateTime endDate, String brand, int categoryID, int stock, int Oprice, int newPrice, string dsc, bool approved)
        {
            this.couponID = couponID;
            this.name = name;
            startDate = start;
            this.endDate = endDate;
            Brand = brand;
            this.categoryID = categoryID;
            this.stock = stock;
            NumOfOrders = 0;
            OriginalPrice = Oprice;
            NewPrice = newPrice;
            Descriptions = dsc;
            Approved = approved;
        }
    }
}
